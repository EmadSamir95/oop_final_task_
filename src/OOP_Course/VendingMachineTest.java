package OOP_Course;

import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class VendingMachineTest {

    private static VendingMachine vm;

    public static void setUp()

    {
        vm = VendingMachineFactory.createVendingMachine();
        System.out.println("Vending Machine instantiated ");
    }


    public static void tearDown()

    {
        vm = null;
    }

    public void testBuyItemWithExactPrice()

    {
        //select item, price in cents
        System.out.println("in this case you try to buy COKE and you have quarter, no problem");
        long price = vm.selectItemAndGetPrice(Item.COKE);
        assertEquals(Item.COKE.getPrice(), price);
        vm.insertCoin(Coin.QUARTER);

        Bucket<Item, List<Coin>> bucket = vm.collectItemAndChange();
        Item item = bucket.getFirst();
        List<Coin> change = bucket.getSecond();

        assertEquals(Item.COKE, item);
        assertTrue(change.isEmpty());
    }

    public void testBuyItemWithMorePrice()

    {
        System.out.println("in this case you insert dollar and bought SODA by 45, so you have more than needed ");
        long price = vm.selectItemAndGetPrice(Item.SODA);
        assertEquals(Item.SODA.getPrice(), price);
        vm.insertCoin(Coin.DOLLAR);
        Bucket<Item, List<Coin>> bucket = vm.collectItemAndChange();
        Item item = bucket.getFirst();
        List<Coin> change = bucket.getSecond();

        assertEquals(Item.SODA, item);
        assertTrue(!change.isEmpty());
        assertEquals(50 - Item.SODA.getPrice(), getTotal(change));

    }

    public void testRefund()

    {
        long price = vm.selectItemAndGetPrice(Item.PEPSI);
        assertEquals(Item.PEPSI.getPrice(), price);
        vm.insertCoin(Coin.DIME);
        vm.insertCoin(Coin.NICKLE);
        vm.insertCoin(Coin.PENNY);
        vm.insertCoin(Coin.QUARTER);
        assertEquals(41, getTotal(vm.refund()));
        System.out.println("total fund now is "+getTotal(vm.refund()));
    }

    public void testSoldOut()

    {
        for (int i = 0; i < 5; i++) {
            vm.selectItemAndGetPrice(Item.COKE);
            vm.insertCoin(Coin.QUARTER);
            vm.collectItemAndChange();
        }
    }


    public void testNotSufficientChangeException()

    {
        for (int i = 0; i < 5; i++) {
            vm.selectItemAndGetPrice(Item.SODA);
            vm.insertCoin(Coin.QUARTER);
            vm.insertCoin(Coin.QUARTER);
            vm.collectItemAndChange();
            vm.selectItemAndGetPrice(Item.PEPSI);
            vm.insertCoin(Coin.QUARTER);
            vm.insertCoin(Coin.QUARTER);
            vm.collectItemAndChange();
        }
    }

    public void testReset()

    {
        VendingMachine vmachine = VendingMachineFactory.createVendingMachine();
        vmachine.reset();
        vmachine.selectItemAndGetPrice(Item.COKE);
    }


    public void testVendingMachineImpl()

    {
        VendingMachineImpl vm = new VendingMachineImpl();
    }


    private long getTotal(List<Coin> change)

    {
        long total = 0;
        for (Coin c : change) {
            total = total + c.getDenomination();
        }
        return total;
    }

}
