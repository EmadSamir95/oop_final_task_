package OOP_Course;

public enum Coin {

        DIME(10), NICKLE(5), PENNY(1), QUARTER(25) , DOLLAR(100) , twoDollars(200);

        private int denomination;

        Coin(int denomination)

        {
            this.denomination = denomination;
        }


        public int getDenomination()

        {
            return denomination;
        }
}
