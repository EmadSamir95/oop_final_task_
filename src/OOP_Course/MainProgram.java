package OOP_Course;

public class MainProgram {

    public static void main(String[] args) {

        VendingMachineTest test = new VendingMachineTest();
        VendingMachineTest.setUp();
        test.testBuyItemWithExactPrice();
        test.testBuyItemWithMorePrice();
        test.testRefund();
        test.testReset();
        test.testNotSufficientChangeException();
        test.testSoldOut();
        VendingMachineTest.tearDown();

    }
}
