package OOP_Course;

public enum Item {

    CANDY("candy",10), SNACK("snack",50), NUTS("nuts",90), COKE("coke",25), PEPSI("pepsi",35), SODA("soda",45);

    private String name;
    private int price;

    Item(String name, int price)

    {
        this.name = name;
        this.price = price;
    }

    public String getName()

    {
        return name;
    }

    public long getPrice()

    {
        return price;
    }

}
